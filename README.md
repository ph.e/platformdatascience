# Projet bis

Session coaching du lundi 27 mai

Je suis un nouveau projet hébergé sous gitlab.

# Accès

Démarrage de l'environnement :
http://platform.phec.net/v2/gl/ph.e%2Fplatformdatascience/master

Démarrage de l'environnement en pointant directement sur RStudio :
http://platform.phec.net/v2/gl/phec.net%2Fplatformdatascience/master?urlpath=rstudio

Démarrage de l'environnement en pointant directement sur RShiny :
http://platform.phec.net/v2/gl/phec.net%2Fplatformdatascience/master?urlpath=shiny/ACM-dashboard/

Démarrage de l'environnement en pointant directement sur RShiny :
http://platform.phec.net/v2/gl/phec.net%2Fplatformdatascience/master?urlpath=shiny/bus-dashboard/

Démarrage de l'environnement en pointant directement sur Dash :
http://platform.phec.net/v2/gl/phec.net%2Fplatformdatascience/master?urlpath=proxy/8050/dash-vanguard-report

| **Url**                          | **Nom**    |
| -------------------------------- | ---------- |
| /tree                            | Notebook   |
| /rstudio                         | RStudio    |
| /shiny/ACM-dashboard/            | RShiny ACM |
| /shiny/bus-dashboard/            | RShiny Bus |
| /proxy/8050/dash-vanguard-report | Dash       |

# Test d'utilisation

## Installation d'un nouveau dash RShiny avec installation de nouveaux paquets R

Cela est simple.

Depuis **Notebook - /tree**, créer un nouveau dossier, par exemple **crandash** et déposez-y les fichiers R de votre dashboard RShiny.

Connectez-vous sur **RStudio - /rstudio** et lancez les installations des paquets directement dans la console, par exemple :

```bash
install.packages(c("dplyr", "htmlwidgets", "digest", "bit"))
devtools::install_github("jcheng5/bubbles")
devtools::install_github("hadley/shinySignals")
```

Connectez-vous enfin sur votre Dash directement via l'url **/shiny/crandash**.

Un fichier log est créé afin de vous aider en cas d'erreur, accessible depuis **Notebook - /tree/logs**.

## Installation d'un nouveau dash Dash en plus de celui existant

## TODO:

FactoShiny : http://factominer.free.fr/graphs/factoshiny.html
