FROM registry.gitlab.com/phec.net/oci/platformbase:base

USER root

RUN rm -r /home/jovyan/*

COPY . /home/jovyan/

RUN chown -R 1000:1000 /home/jovyan/

USER ${NB_USER}

# Opération si nécessaires : PIP install, R install

# R      - DataTable library
RUN Rscript -e 'install.packages("DT")'

CMD jupyter notebook --ip 0.0.0.0
